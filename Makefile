PROJ = eli

DATA_PATH = data/
WWW_PATH = www/

DATA = script.python/$(PROJ).csv data/$(PROJ).RData
WWW = www/ps-model-for_0-5_risk-cc.html www/ps-model-for_5-10_risk-cc.html \
      www/ps-model-for_5-7.5_risk-cc.html www/ps-model-for_7.5-10_risk-cc.html

all : $(DATA) $(WWW)

### Generació python
script.python/$(PROJ).csv : script.python/00-generation.py defs/problems
	python script.python/00-generation.py > $(WWW_PATH)generation.txt

### Inicialització
data/$(PROJ).RData : script.R/run-knit2html.R script.R/01-initialize.Rmd script.python/$(PROJ).csv defs/eps defs/problems defs/meds
	mkdir -p .tmp/$@ 
	Rscript -e 'OUT = ".tmp/$@/01-initialize.html"; IN = "script.R/01-initialize.Rmd"; source("script.R/run-knit2html.R")'
	mv .tmp/$@/01-initialize.html www/

www/ps-model-for_0-5_risk-cc.html : script.R/run-knit2html.R script.R/02-ps-model-for_risk_interval-cc.Rmd data/$(PROJ).RData
	mkdir -p .tmp/$@ 
	Rscript -e 'RISK_MIN = 0; RISK_MAX = 5; OUT = ".tmp/$@/$(@F)"; IN = "script.R/02-ps-model-for_risk_interval-cc.Rmd"; source("script.R/run-knit2html.R")'
	mv .tmp/$@/$(@F) www/

www/ps-model-for_5-10_risk-cc.html : script.R/run-knit2html.R script.R/02-ps-model-for_risk_interval-cc.Rmd data/$(PROJ).RData
	mkdir -p .tmp/$@ 
	Rscript -e 'RISK_MIN = 5; RISK_MAX = 10; OUT = ".tmp/$@/$(@F)"; IN = "script.R/02-ps-model-for_risk_interval-cc.Rmd"; source("script.R/run-knit2html.R")'
	mv .tmp/$@/$(@F) www/

www/ps-model-for_5-7.5_risk-cc.html : script.R/run-knit2html.R script.R/02-ps-model-for_risk_interval-cc.Rmd data/$(PROJ).RData
	mkdir -p .tmp/$@ 
	Rscript -e 'RISK_MIN = 5; RISK_MAX = 7.5; OUT = ".tmp/$@/$(@F)"; IN = "script.R/02-ps-model-for_risk_interval-cc.Rmd"; source("script.R/run-knit2html.R")'
	mv .tmp/$@/$(@F) www/

www/ps-model-for_7.5-10_risk-cc.html : script.R/run-knit2html.R script.R/02-ps-model-for_risk_interval-cc.Rmd data/$(PROJ).RData
	mkdir -p .tmp/$@ 
	Rscript -e 'RISK_MIN = 7.5; RISK_MAX = 10; OUT = ".tmp/$@/$(@F)"; IN = "script.R/02-ps-model-for_risk_interval-cc.Rmd"; source("script.R/run-knit2html.R")'
	mv .tmp/$@/$(@F) www/
