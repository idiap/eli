# -*- coding: utf-8 -*-

from nova2.population import Population
from nova2.problems import Problems
from nova2.treatment import Treatment
from nova2.cohort import Cohort
from nova2.variable import Variable
from nova2.visits import Visits
from nova2.eq import EQ
from nova2.paths import Paths
from utils import misc
import datetime
import numpy as np

# Study period
begin_study = datetime.date(2006,7,1)
end_study   = datetime.date(2012,12,31)

print "Begin study:"
print begin_study
print "End study:"
print end_study

# Cohort intro period
intro_from = datetime.date(2006,7,1)
intro_until = datetime.date(2012,12,31)

print "Begin entrance:"
print intro_from
print "End entrance:"
print intro_until

# Population age
#min_age = 35
#max_age = 75

#print "Min age:"
#print min_age
#print "Max age:"
#print max_age

######## Selecció població 
paths = Paths('2013')

# Initial population
population = Population(filename = paths.get('sidiapq'), cmbd_path=paths.get('cmbd'))


# Problems creation using population
problems = Problems(paths)

diab2 = problems.get_events(population, 'diabetes_t2')

population = Population(filename = paths.get('sidiapq'), filtering = diab2, cmbd_path=paths.get('cmbd'))

from collections import Counter
def count_by_years(dd):
  ddd = dict(Counter(dd))
  accum = 0
  for f,c in sorted(ddd.items()):
    accum = accum + c
    print "%s:%8d\t%8d" % (str(f),c,accum)
  print ""

# Defining the cohort
cohort = Cohort(population = population, 
                beginning = begin_study, 
                ending = end_study, 
                minIntro = intro_from, 
                maxIntro = intro_until)

d_diab2 = problems.get_absolute_minimum(diab2)
cohort.setMinIntroDate(d_diab2)

cohort.setStatusFilter(population)    # Alive, death or tranferred filter

#cohort.setAgeFilter(population, min_age, max_age)  # Age filter
#print "Entrance of population within age range:"
#count_by_years( dict(Counter([cohort._minIntro[o].year for o in cohort.cohort])) )

# Exclusion 
l_antecedents = ['ami_anni', 'angor', 'stroke_i', 'stroke_e', 'stroke_h', 'pad', 'tia', 'ihd_acute', 'ihd_chronic',
                 'gangrene', 'amputation']
l_procedures = ['card_proc']

#### Filters applied to the cohort
ev = problems.get_events(cohort.cohort, l_antecedents)
cohort.setMaxIntroDate( problems.get_absolute_minimum(ev) ) # Cardio-vascular filter
pr = problems.get_procedures(cohort.cohort, l_procedures)
cohort.setMaxIntroDate( misc.addToDate(problems.get_absolute_minimum(pr), datetime.timedelta(-1)) ) # procedure filter
cardio_event = misc.getMinDate(problems.get_absolute_minimum(ev), problems.get_absolute_minimum(pr))

print "Entrance of population alive and not translated without CV event:"
intros = [ "%4d-%02d" % (cohort._minIntro[o].year, cohort._minIntro[o].month) for o in cohort.cohort]
count_by_years(intros)

#print "Entrance of population without CV registry:"
#count_by_years( dict(Counter([cohort._minIntro[o].year for o in cohort.cohort])) )

cohort.setIntro()

# Months without statine for statine beginning and ending
stat_months_beg = 6
stat_months_end = 6

print "Statine months beginning:"
print stat_months_beg
print "Statine months ending:"
print stat_months_end

#### Eliminating all-period user
statines = Treatment(population = cohort.cohort, filename = paths.treatment('statine'), catalog = paths.get('catalog') )

prevalent = statines.statusAt(cohort.cohortIntro)
print "Prevalent statine users:"
print Counter([prevalent[o] for o in prevalent])

bef_beginning_all = statines.lastBeginning(cohort.cohort, months = stat_months_end, getAll=True, before = cohort.cohortIntro)
bef_beginning = {o: bef_beginning_all[o][0] for o in bef_beginning_all}
bef_ending = statines.firstEnding(bef_beginning, months = stat_months_end, after = bef_beginning)

aft_beginning_all = statines.firstBeginning(cohort.cohort, months = stat_months_end, getAll=True, after = cohort.cohortIntro, before = misc.addToDate(cohort._maxIntro, datetime.timedelta(-30)))
aft_beginning = {o: aft_beginning_all[o][0] for o in aft_beginning_all}
aft_ending = statines.firstEnding(aft_beginning, months = stat_months_end, after = aft_beginning)

## S'eliminen els usuaris prevalents
#cohort.eliminate( [ o for o in prevalent if prevalent[o] == 1 ])


cohort.addData(cohort.cohortIntro, 'dintro')

cohort.addData(population.ageAt(cohort.cohortIntro), 'age')
cohort.addData(population.sex(cohort.cohort), 'sex')

### End-points avoided M months after statine beginning
##M = 3
##hard_ep = ['ami', 'angor', 'stroke_i', 'stroke_e', 'tia', 'pad']
###M = 1
###hard_ep = ['ami', 'angor', 'stroke_i', 'stroke_e', 'tia']
##print "Minimum gap between statine beginning and hard event ['ami', 'angor', 'stroke_i', 'stroke_e', 'tia'] (months):"
##print M
##ev = problems.get_events(cohort.cohort, hard_ep)
##hard_ev = problems.get_absolute_minimum(ev)
##max_beginning  = misc.getMinDate(cohort._exitus, misc.addToDate(hard_ev, datetime.timedelta( -30 * M )))


#beginning_all = statines.firstBeginning(cohort.cohort, months = stat_months_end, after = cohort._minIntro, before = max_beginning, getAll=True)
#beginning = {o: beginning_all[o][0] for o in beginning_all}
#ending = statines.firstEnding(beginning, months = stat_months_end, after = beginning)

print "Statine beginnings distribution after 2006-07-01:"
begs = [ "%4d-%02d" % (aft_beginning[o].year, aft_beginning[o].month) for o in aft_beginning]
count_by_years(begs)

print "Statine endings distribution after 2006-07-01:"
ends = [ "%4d-%02d" % (aft_ending[o].year, aft_ending[o].month) for o in aft_ending]
count_by_years(ends)

cohort.addData(bef_beginning_all, ['stat.beg_bef', 'stat.atc_bef', 'stat.env_bef', 'stat.comp_bef', 'stat.dose_bef'])
cohort.addData(bef_ending, 'stat.end_bef')

cohort.addData(aft_beginning_all, ['stat.beg', 'stat.atc', 'stat.env', 'stat.comp', 'stat.dose'])
cohort.addData(aft_ending, 'stat.end')

mpr = statines.mpr(aft_beginning, dfrom=aft_beginning, duntil=aft_ending)
mpr6m = statines.mpr(aft_beginning, dfrom=aft_beginning, duntil=misc.addToDate(aft_beginning, datetime.timedelta(6*30)))
mpr1y = statines.mpr(aft_beginning, dfrom=aft_beginning, duntil=misc.addToDate(aft_beginning, datetime.timedelta(12*30)))

cohort.addData(mpr, 'mpr')
cohort.addData(mpr6m, 'mpr6m')
cohort.addData(mpr1y, 'mpr1y')

cohort.addData(misc.getValue(paths.get('medea')), 'medea')
cohort.addData(misc.getValue(paths.get('medea'), ivalue=2), 'rural')

eq = EQ(population, eqafile = paths.get('eqa').replace('2013', '2012'), 
                    eqffile = paths.get('eqf').replace('2013', '2012'), 
                    relfile = paths.get('rel').replace('2013', '2012'))
cohort.addData(eq.getEQs(cohort.cohortIntro), ['UBA', 'EQA', 'EQF'])

cohort.addData(cohort._exitus_reason, 'exitus')
cohort.addData(cohort._exitus, 'dexitus')

#visits = Visits(paths.get('visits').replace('2013', '2012'))
#cohort.addData( visits.numbervisits(cohort.cohortIntro) , 'visits', default_na = 0)

cohort.addBinaryData(misc.getSet(paths.get('visits').replace('2013', '2012'), 1), 'with.visit')

# Problems at dintro
#probs_list = ['smoking', 'obesity', 'htn_not_org', 'aff', 'dyslipidemia', 'mr_eli', 'alcoholism', 'arthritis_eli',
#              'lupus', 'asthma', 'copd', 'cancer', 'neoplasms_malignant', 'neoplasms_benign', 'copd_219',
#              'acute_liver_disease', 'alt_metabol_endocri', 'depression']
probs_list = misc.read_vector('defs/problems')
# 'ard', 'obesity', 'diabetes', 'diabetes_2', 'alcoholism', 'htn', 
#              'aff', 'lupus', 'dyslipidemia', 'arthritis', 'asthma', 'copd', 'hepatopathy', 
#              'acute_liver_disease', 'chronic_liver_disease', 'hypotirodism', 'miopathy',
#              'depression', 'cancer', 'neoplasms_malignant', 'neoplasms_benign']

problems.filtering(cohort.cohort)

evts = problems.get_events(cohort.cohort, probs_list, before=cohort.cohortIntro)
cohort.addBinaryData(evts, probs_list)


# Variables at dintro
alcohol = Variable(population = cohort.cohort, filename = paths.variable('alcohol'))
cohort.addData(alcohol.lastMeasure(cohort.cohort, before=cohort.cohortIntro), ['dalcohol', 'alcohol'])
cohort.addData(alcohol.firstMeasure(cohort.cohort, after=cohort.cohortIntro), ['dalcohol.post', 'alcohol.post'])

tabac = Variable(population = cohort.cohort, filename = paths.variable('tabac'))
cohort.addData(tabac.lastMeasure(cohort.cohort, before=cohort.cohortIntro), ['dtabac', 'tabac'])
cohort.addData(tabac.firstMeasure(cohort.cohort, after=cohort.cohortIntro), ['dtabac.post', 'tabac.post'])

vnames = ['pes', 'talla', 'imc', 'glu', 'hba1c', 'cre']

gap = datetime.timedelta(12 * 30)
print "Gap before and after for measurements (days):"
print gap.days

max_measure = dict()
for o in cohort.cohort:
  max_measure[o] = intro_from + gap
  if o in cardio_event:
    max_measure[o] = min(max_measure[o], cardio_event[o])
  if o in aft_beginning:
    max_measure[o] = min(max_measure[o], aft_beginning[o])

v_lim = { v: {'min': intro_from - gap, 'max': max_measure} for v in vnames + ['col', 'bp', 'sb']}
v_lim['talla']['max'] = end_study
v_lim['pes']['max'] = end_study
v_lim['imc']['max'] = end_study

### TAS i TAD
tas = Variable(population = cohort.cohort, filename = paths.variable('tas')).allMeasures(cohort.cohort, after = v_lim['bp']['min'], before = v_lim['bp']['max'], as_dict=True)
tad = Variable(population = cohort.cohort, filename = paths.variable('tad')).allMeasures(cohort.cohort, after = v_lim['bp']['min'], before = v_lim['bp']['max'], as_dict=True)

tensions = misc.nearest(cohort.cohortIntro, misc.mergeMeasurements(tas, tad))
cohort.addData(tensions , ['tas', 'tad'])

print "Mesures tensió: %.2f%% [%d]" % (float(len(tensions)) / len(cohort.cohort) * 100, len(tensions))
print "Mesures distribució: 'tas', 'tad'"
count_by_years( dict(Counter([''.join(map(lambda j: '1' if j != 'NA' else '0', tensions[o])) for o in tensions])) )

### COLTOT, COLHDL, COLLDL i TG
cols = ['coltot', 'colhdl', 'colldl', 'tg']
cols_dict = dict()
for v in cols:
  cols_dict[v] = Variable(population = cohort.cohort, filename = paths.variable(v)).allMeasures(cohort.cohort, after = v_lim['col']['min'], before = v_lim['col']['max'], as_dict=True)
  print "%s: %.2f%%" % (v, float(len(cols_dict[v])) / len(cohort.cohort) * 100)

colesterols = misc.nearest(cohort.cohortIntro, misc.mergeMeasurements(cols_dict['coltot'], cols_dict['colhdl'], cols_dict['colldl'], cols_dict['tg']))
cohort.addData(colesterols , cols)

print "Mesures colesterols: %.2f%% [%d]" % (float(len(colesterols)) / len(cohort.cohort) * 100, len(colesterols))
print "Mesures distribució: 'coltot', 'colhdl', 'colldl', 'tg'"
count_by_years( dict(Counter([''.join(map(lambda j: '1' if j != 'NA' else '0', colesterols[o])) for o in colesterols])) )


### S'agafen colesterols sense restricció
#for vname in cols:
#    measurements = Variable(population = cohort.cohort, filename = paths.variable(vname)).allMeasures(cohort.cohort, after = v_lim['col']['min'], before = v_lim['col']['max'], as_dict=True)
#    nearst_msr = misc.nearest(cohort.cohortIntro, measurements)
#    ### S'ha de fer la mitjana de les mesures que hi ha en l'ultim analisis
#    uni_measure = { o: round(np.mean( map(float, nearst_msr[o])), 2) for o in nearst_msr }
#    cohort.addData(uni_measure, vname + '.unrestricted')
#    print "%s: %.2f%%" % (vname + '.unrestricted', float(len(uni_measure)) / len(cohort.cohort) * 100)


### leucocits basofils_p eosinofils_p limfocits_p monocits_p neutrofils_p
sb = ['leucocits', 'basofils_p', 'eosinofils_p', 'limfocits_p', 'monocits_p', 'neutrofils_p']
sb_dict = dict()
for v in sb:
  sb_dict[v] = Variable(population = cohort.cohort, filename = paths.variable(v)).allMeasures(cohort.cohort, after = v_lim['sb']['min'], before = v_lim['sb']['max'], as_dict=True)
  print "%s: %.2f%%" % (v, float(len(sb_dict[v])) / len(cohort.cohort) * 100)

serie_blanca = misc.nearest(cohort.cohortIntro, misc.mergeMeasurements(sb_dict['leucocits'], sb_dict['basofils_p'], sb_dict['eosinofils_p'], sb_dict['limfocits_p'], sb_dict['monocits_p'], sb_dict['neutrofils_p']))
cohort.addData(serie_blanca , sb)

print "Mesures serie blanca: %.2f%% [%d]" % (float(len(serie_blanca)) / len(cohort.cohort) * 100, len(serie_blanca))
print "Mesures distribució: 'leucocits', 'basofils_p', 'eosinofils_p', 'limfocits_p', 'monocits_p', 'neutrofils_p'"
count_by_years( dict(Counter([''.join(map(lambda j: '1' if j != 'NA' else '0', serie_blanca[o])) for o in serie_blanca])) )


### Altres variables
for vname in vnames:
    measurements = Variable(population = cohort.cohort, filename = paths.variable(vname)).allMeasures(cohort.cohort, after = v_lim[vname]['min'], before = v_lim[vname]['max'], as_dict=True)
    nearst_msr = misc.nearest(cohort.cohortIntro, measurements)
    ### S'ha de fer la mitjana de les mesures que hi ha en l'ultim analisis
    uni_measure = { o: round(np.mean( map(float, nearst_msr[o])), 2) for o in nearst_msr }
    cohort.addData(uni_measure, vname)
    print "%s: %.2f%%" % (vname, float(len(uni_measure)) / len(cohort.cohort) * 100)



# Facturation at dintro (variable indicates if a facturation of given med exists during
# last i months 
meds_persistence = {'aspirine': 3, 'c10-no-statine': 6, 'g03a': 3,  'c02': 3, 'c03': 3, 'c07': 3,
                    'c08': 3, 'c09': 3, 'a10': 3, 'n06': 6, 'm01': 3, 'n05': 6, 'n06' : 6, 'h02': 3, 'b01' : 3 }
print "Persistencia de medicació (mesos):"
print meds_persistence

for m in meds_persistence:
    print m
    treatment = Treatment(population = cohort.cohort, filename = paths.treatment(m), catalog = paths.get('catalog') )
    effect = datetime.timedelta(30 * meds_persistence[m])
    cohort.addData(treatment.statusAt(cohort.cohortIntro, effect = effect), m, default_na = '0')


### Incidences
# Problem incidence
hard_ep = ['ami', 'angor', 'stroke_i', 'stroke_e', 'tia', 'pad']
incidence_probs = hard_ep + ['ami_anni', 'stroke', 'sudden_death', 'sudden_death_card', 'adverse_effects', 'miopathy', 'ard', 'cataract']

evts = problems.get_events(cohort.cohort, incidence_probs, after = cohort.cohortIntro, before = cohort._exitus)
cohort.addData(problems.get_relative(evts, incidence_probs, which=0), ['ep_' + i for i in incidence_probs], default_na = 'NA')

# Problem incidence (cmbd)

evts = problems.get_events(cohort.cohort, hard_ep, after = cohort.cohortIntro, before = cohort._exitus, use_ecap = False)
cohort.addData(problems.get_relative(evts, hard_ep, which=0), ['ep_' + i + '.cmbd' for i in hard_ep], default_na = 'NA')

### Procedures
# Procedure incidence
incidence_procs = ['card_proc', 'peri_proc', 'stroke_proc']

procs = problems.get_procedures(cohort.cohort, incidence_procs, after = cohort.cohortIntro, before = cohort._exitus)
cohort.addData(problems.get_relative(procs, incidence_procs, which=0), ['ep_' + i for i in incidence_procs], default_na = 'NA')

cohort.writeTable("script.python/eli.csv")

